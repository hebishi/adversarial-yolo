# Adversarial YOLO
This repository id based on the marvis YOLOv2 inplementation: https://github.com/marvis/darknet

This work corresponds to the following paper:
```
@inproceedings{lin2014microsoft,
    title={Fooling automated surveillance cameras: adversarial patches to attack person detection},
    author={Thys, Simen and Van Ranst, Wiebe and Goedem\'e, Toon},
    booktitle={CVPRW: Workshop on The Bright and Dark Sides of Computer Vision: Challenges and Opportunities for Privacy and Security},
    year={2019}
}
```


If you use this work, please cite this paper.
